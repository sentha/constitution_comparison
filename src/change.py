from json import dump
from os import listdir
from re import sub

from grundgesetz import _paragraph_divider, export_path

_consecutive_spaces = r"\s\s+"
_not_number = r"[^\d]"


class Change:
    def __init__(self, old_gg, new_gg):
        """Most of the constructor is resetting vars, to make sure they are not of an unexpected value."""
        self.old_gg = old_gg
        self.new_gg = new_gg

        self.old_articles = self.old_gg.data_dict["articles"]
        self.new_articles = self.new_gg.data_dict["articles"]

        self._metadata_change = []

        self._removed_articles = []
        self._added_articles = []
        self._changed_articles = {}

        self._deleted_paragraphs = {}
        self._added_paragraphs = {}
        self._changed_paragraphs = {}
        self._total_deleted_paragraphs = 0
        self._total_added_paragraphs = 0
        self._total_changed_paragraphs = 0

        self._total_old_words = 0
        self._total_new_words = 0
        self._total_diff_words = 0

        self.results = {"metadata": {}, "articles": {}, "paragraphs": {}}

    def compare(self):
        """This method runs all comparison methods."""
        # compare the metadata part of both data-dicts and collects new metadata from it
        self._compare_metadata()

        # compare the article part of both data-dicts and collects new metadata from it
        self._compare_articles()

        # compares the paragraphs of both data-dicts and collects new metadata from it
        self._compare_paragraphs()

    def _compare_metadata(self):
        """This method compares metadata of the grundgesetz objects.
        Currently it only detects change in the preamble."""
        old_metadata = self.old_gg.data_dict["metadata"]
        new_metadata = self.new_gg.data_dict["metadata"]
        for key in old_metadata.keys():
            if old_metadata[key] != new_metadata[key]:
                self._metadata_change.append(key)

    def _compare_articles(self):
        """This method counts and lists, added, deleted and changed articles
        and counts the words for each changed article."""
        # check for removed articles, all others are candidate for change
        for article in self.old_articles:
            if article not in self.new_articles:
                self._removed_articles.append(article)
            else:
                # check which of the not removed articles have changes in them
                if self.old_articles[article] != self.new_articles[article]:
                    self._changed_articles[article] = {}

        # check for new articles
        for article in self.new_articles:
            if article not in self.old_articles:
                self._added_articles.append(article)

        for article in self._changed_articles:
            current_old_body = self.old_articles[article]["body"]
            current_new_body = self.new_articles[article]["body"]

            # remove the paragraph dividers as they are not part of the word count as well as consecutive spaces
            current_old_body = sub(_consecutive_spaces, "", current_old_body.replace(_paragraph_divider, ""))
            current_new_body = sub(_consecutive_spaces, "", current_new_body.replace(_paragraph_divider, ""))

            # count the words
            old_body_wordcount = len(current_old_body.split(" "))
            new_body_wordcount = len(current_new_body.split(" "))
            diff_body_wordcount = new_body_wordcount - old_body_wordcount

            self._changed_articles[article] = {
                "old": old_body_wordcount,
                "new": new_body_wordcount,
                "diff": diff_body_wordcount
            }

            self._total_old_words += old_body_wordcount
            self._total_new_words += new_body_wordcount
            self._total_diff_words += diff_body_wordcount


    def _compare_paragraphs(self):
        """This method counts and lists the deleted, added and otherwise changed paragraphs for each article."""
        for article in self._changed_articles:
            current_old_paragraphs = self.old_articles[article]["paragraphs"]
            current_added_paragraphs = self.new_articles[article]["paragraphs"]

            old_size = len(current_old_paragraphs)
            new_size = len(current_added_paragraphs)

            if old_size > new_size:
                self._deleted_paragraphs[article] = [_ for _ in range(new_size + 1, old_size + 1)]
                self._total_deleted_paragraphs = old_size - new_size
            elif new_size > old_size:
                self._added_paragraphs[article] = [_ for _ in range(old_size + 1, new_size + 1)]
                self._total_added_paragraphs += new_size - old_size
            else:
                # If there is no paragraph, we don't count the change.
                if old_size != 0:
                    self._changed_paragraphs[article] = [_ for _ in range(1, old_size + 1)]
                    self._total_changed_paragraphs += old_size

    def print_results(self):
        """This prints the results in a pretty way."""
        # metadata change
        metadata = "The following metadata changed: \n{}".format(_custom_list_format(self._metadata_change))

        # paragraph change
        paragraphs_deleted = "The following {} articles' paragraphs have been deleted: {}" \
            .format(self._total_deleted_paragraphs, _custom_dict_format(self._deleted_paragraphs))
        paragraphs_new = "The following {} articles' paragraphs have been added: {}" \
            .format(self._total_added_paragraphs, _custom_dict_format(self._added_paragraphs))
        paragraphs_changed = "The following {} articles' paragraphs have been changed: {}" \
            .format(self._total_changed_paragraphs, _custom_dict_format(self._changed_paragraphs))

        # articles change
        articles_removed = "The following {} articles have been removed: \n{}" \
            .format(len(self._removed_articles), _custom_list_format(self._removed_articles))
        articles_added = "The following {} articles have been added: \n{}" \
            .format(len(self._added_articles), _custom_list_format(self._added_articles))
        articles_changed = "The following {} articles have been changed: {}" \
            .format(len(self._changed_articles), _custom_dict_format(self._changed_articles))

        old_words = "The old document has {} words.".format(self._total_old_words)
        new_words = "The new document has {} words.".format(self._total_new_words)
        diff_words = "That's a difference of {} words."\
            .format("+" + str(self._total_diff_words) if self._total_diff_words > 0 else self._total_diff_words)

        # make this a nice string and print it
        print("\n\n".join([
            metadata,
            paragraphs_deleted, paragraphs_new, paragraphs_changed,
            articles_removed, articles_added, articles_changed,
            old_words, new_words, diff_words
        ]))

    def export_results(self):
        """This method exports the results as json to parsed/json/change.json"""
        # save data in a dict
        self.results["metadata"]["from_date"] = sub(_not_number, "", self.old_gg.filename)
        self.results["metadata"]["to_date"] = sub(_not_number, "", self.new_gg.filename)
        self.results["metadata"]["preamble_changed"] = [True if "preamble" in self._metadata_change else False][0]
        self.results["metadata"]["total_deleted_paragraphs"] = self._total_deleted_paragraphs
        self.results["metadata"]["total_added_paragraphs"] = self._total_added_paragraphs
        self.results["metadata"]["total_changed_paragraphs"] = self._total_changed_paragraphs
        self.results["metadata"]["total_old_words"] = self._total_old_words
        self.results["metadata"]["total_new_words"] = self._total_new_words
        self.results["metadata"]["total_diff_words"] = self._total_diff_words

        self.results["articles"]["deleted"] = self._removed_articles
        self.results["articles"]["added"] = self._added_articles
        self.results["articles"]["changed"] = self._changed_articles

        self.results["paragraphs"]["deleted"] = self._deleted_paragraphs
        self.results["paragraphs"]["added"] = self._added_paragraphs
        self.results["paragraphs"]["changed"] = self._changed_paragraphs

        # check if there is already a change file and generate the output file string accordingly
        existing_files = listdir(export_path)
        filename = "change.json"
        counter = 0
        while filename in existing_files:
            counter = counter + 1
            filename = "change_{}.json".format(counter)

        # write the results to the file in json format
        with open(export_path + filename, "w") as f:
            dump(self.results, f, indent=4, ensure_ascii=False)

        print("\nExport complete. You can find the intermediate results "
              "under 'parsed/raw' and the final results under 'parsed/json'.\n")


def _custom_dict_format(old_dict_format):
    """This function adds newlines to a dict and removes ugly braces in dicts for pretty output."""
    if old_dict_format == {}:
        return "None"
    return str(old_dict_format) \
        .replace("}, ", "\n") \
        .replace("], ", "\n") \
        .replace("{", "\n") \
        .replace("[", "\n") \
        .strip("}").strip("]")


def _custom_list_format(old_list_format):
    """This function adds newlines to a dict and removes ugly braces in lists for pretty output."""
    if not old_list_format:
        return "None"
    return str(old_list_format) \
        .replace(", ", "\n") \
        .strip("[") \
        .strip("]")


if __name__ == "__main__":
    print("Whoops, looks like you started the wrong script. Make sure to start src/main.py, not {}.".format(__file__))
