from json import dump
from re import split as re_split

# For a detailed explanation or testing of the RegEx below, use https://regex101.com/
_article_divider = r"(Art(?:icle)?\s\d{1,3}[a-z]?\n)"
"""
RegEx test-data for _article_divider:
The following should match:
Article 1
Article 12
Article 123
Art 1
Art 12
Art 123
Article 1a
Article 12b
Article 123c
Art 1a
Art 12b
Art 123c

The following should not match:
Article 1234
Art 1234
Article 1234d
Art 1234d
Artikel 1234
Artikel 1234d
"""

export_path = "parsed/json/"

_paragraph_divider = r"\(\d+\)\s?"


class Grundgesetz:
    # noinspection SpellCheckingInspection
    """This class analyses a Grundgesetz that is in a txt format assuming the file is in a folder '~/unparsed'."""
    # public attributes
    filename = None

    old_path = "unparsed/"
    new_path = "parsed/raw/"

    def __init__(self, fn):
        self.filename = fn
        self.old_path = self.old_path + self.filename
        self.new_path = self.new_path + self.filename

        # needed for resetting to avoid unexpected behaviour
        self.data_dict = {}

        # private attributes
        self._article_heads = None
        self._article_bodies = None
        self._article_paragraphs = None
        self._metadata_dict = {}
        self._articles_dict = {}

    def parse(self):
        """Does all reading work for a Grundgesetz converting an unformatted .txt to a useful json."""
        # parsing
        self._remove_empty_lines()

        # reading
        self._split_by_articles()
        self._split_by_paragraphs()

        # converting to dict
        self._convert_to_dict()

        # export to json
        self._export_to_json()

    def _remove_empty_lines(self):
        """Using the objects path, open all files one by one, remove empty lines and then write it to a new file."""
        with open(self.old_path) as input_file, open(self.new_path, 'w') as output_file:
            output_file.writelines(line for line in input_file if line.strip())
            output_file.truncate()

    def _split_by_articles(self):
        """Opens the parsed Files, splits by articles and splits the articles by header and body."""
        with open(self.new_path) as input_file:
            parsed_articles = re_split(_article_divider, input_file.read())
            self._metadata_dict["preamble"] = parsed_articles[0]
            self._article_heads = [newline_head.strip("\n").strip() for newline_head in parsed_articles[1::2]]
            self._article_bodies = [article_body.replace("\n", " ").strip() for article_body in parsed_articles[2::2]]

    def _split_by_paragraphs(self):
        """Uses the parsed articles bodies to parse the paragraphs out of them and then add them to the dict."""
        self._article_paragraphs = [
            [paragraph.strip() for paragraph in paragraphs[1:]]
            for paragraphs in [re_split(_paragraph_divider, article) for article in self._article_bodies]
        ]

    def _convert_to_dict(self):
        """Converts the collected data to a dict."""
        # adds metadata as list
        self.data_dict["metadata"] = self._metadata_dict

        # generates the articles dict
        for i in range(len(self._article_heads)):
            self._articles_dict[self._article_heads[i]] = \
                {"body": self._article_bodies[i], "paragraphs": self._article_paragraphs[i]}

        # adds previously generated articles dict
        self.data_dict["articles"] = self._articles_dict

    def _export_to_json(self):
        """Exports self.data_dict to a json file named the same as the old file to 'parsed/json/'"""
        with open(export_path + self.filename + ".json", "w+") as f:
            dump(self.data_dict, f, indent=4, ensure_ascii=False)


if __name__ == "__main__":
    print("Whoops, looks like you started the wrong script. Make sure to start src/main.py, not {}.".format(__file__))
