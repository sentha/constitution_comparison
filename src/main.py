from change import Change
from grundgesetz import Grundgesetz


def custom_input(custom_string):
    try:
        return input(custom_string)
    except KeyboardInterrupt:
        print("Goodbye!")
        exit()


if __name__ == "__main__":
    print("Read the README.md file if you encounter any difficulties.")
    while True:
        # old_filename = custom_input("What's the filename of the old gg you want to compare?\nType here: ")
        old_filename = "De1949GG"
        old_gg = Grundgesetz(old_filename)
        old_gg.parse()

        # new_filename = input("What's the filename of the new gg you want to compare?\nType here: ")
        new_filename = "De2020GG"
        new_gg = Grundgesetz(new_filename)
        new_gg.parse()

        results_choice = input("Do you want to print the difference (p), export the results (e) or do both (b)?"
                               "\nType (p/e/b) here: ")

        current_change = Change(old_gg, new_gg)
        current_change.compare()

        # Yes this is a while True, no I don't want to redo it for a cleaner solution, yes this works.
        while True:
            if results_choice == "p":
                current_change.print_results()
                break
            elif results_choice == "e":
                current_change.export_results()
                break
            elif results_choice == "b":
                current_change.print_results()
                current_change.export_results()
                break
            else:
                results_choice = input("You can only choose 'p', 'e' or 'b' as options.\nType (p/e/b) here:")

        break_flag = input("Do you want to continue? (y/n)\nType here: ")
        if break_flag == "n":
            break
        elif break_flag != "y":
            print("Let's count that as a yes. If you meant to exit the program, just press CTL+C or kill the terminal.")
