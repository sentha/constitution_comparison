# constitution comparison script

This program can compare german constitutions and export the data found as practical json.

![Alt text](verfassung_ddr.png "a title")

## Installation
Ensure you have python3 installed via

```
python3 --version
```
The program was developed under Python version 3.8.5, so your version should be at least that.
The project has no external dependencies, so you don't need pip3 or anything else.

Ensure you have the repository downloaded completely.

## Usage example
To use the program make sure to place the grundgesetz-textfiles you want to analyse
under **"unparsed/"**. Make sure the only numbers the filename contains is the constitutions year.
For example: **De1949GG** or **2020GG**. It doesn't matter if you call them .txt or not. Both is fine.

Change directory to the repositories root directory in a new terminal.
Then simply execute:

```sh
python3 src/main.py
```

The program will ask you questions and lead you through the rest of the analysis.

## Meta
Maintainer: Cedric Mertig – [is using git](https://gitlab.com/cl.mertig) -  cl.mertig@gmail.com

Distributed under the WTFPL license. See ``LICENSE`` for more information.

## Contributing
The contributing parties did not want to be mentioned.
